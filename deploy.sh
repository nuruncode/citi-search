#!/bin/bash

function usage
{
  echo ""
  echo "   usage : $0 [-e env]"
  echo ""
  echo "          -e,  --env"
  echo "                 Environment: dev"
  echo ""
}

while [ "$1" != "" ]
do
  case $1 in
     -e | --env )
        shift
        env=`echo $1 | tr '[:upper:]' '[:lower:]'`
        ;;
     -h | --help )
        usage
        exit 1
        ;;
     * )
        usage
        exit 1
        ;;
  esac
  shift
done

if [ "${env}" == "" ]; then
  env="dev"
fi

webserver="web.citi.internal"
ssh_option="-F ssh_config"
appdir_home="/var/www/vhosts/${env}-citi-search.nurunqc.com"
appdir_root="${appdir_home}/dist"
appdir_backup="${appdir_home}/dist.backup"
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
blue='\033[0;34m'
nc='\033[0m'

echo -e "${blue}Deploy static files on webserver${nc}"
takebackup=$(ssh ${ssh_option} ${webserver} "[ -d ${appdir_backup} ] && rm -rf ${appdir_backup}/* && cp -Rf ${appdir_root}/* ${appdir_backup}/")
exitcode=$?
if [ "${exitcode}" != "0" ]; then
  echo -e "${red}Unable to take application backup${nc} ${takebackup}"
  exit 1
else
  echo -e "${green}Backup successfully completed${nc} : ${appdir_backup}"
fi
deployapp=$(rsync -e "ssh ${ssh_option}" -avc ./dist ubuntu@${webserver}:/var/www/vhosts/dev-citi-search.nurunqc.com --delete)
exitcode=$?
if [ "${exitcode}" != "0" ]; then
  echo -e "${red}Unable to deploy application${nc} : ${deployapp}"
  exit 1
else
  echo -e "${green}Application successfully deployed${nc} : ${appdir_root}"
  echo ""
  echo -e "${yellow}https://dev-citi-search.nurunqc.com${nc}"
  echo ""
fi

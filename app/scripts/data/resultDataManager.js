import dispatcher from '../events/dispatcher'
import { 
  ADD_FILTER, 
  REMOVE_FILTER, 
  SALES_FILTER, 
  FILTER_RESET, 
  RESULTS_UPDATE, 
  NO_RESULTS,
  HAS_RESULTS
} from '../events/events'
import config from '../search/config'


// TODO: On Monday
// - implement change event broadcasting
// - implement data filtering when a filter is selected/deselected
// - implement isOnSale functionality


class ResultDataManager {
  constructor() {
    this._originalData = [];
    this._filterPanelData = [];
    this._searchResultsData = [];
    this._isOnSale = false;
    this._hasSales = false;
    this._selectedFilters = [];
    this._callbacks = [];
    this._totalResults = 0;
    this._originalTotalResults = 0;
    this._hasResults = false;
    this.defaultImage = null;

    this._initialize();
  }



  setOriginalData(data) {
    this._originalData = data;
    this._parseOriginalData();
  }

  _addCallback(callback) {
    this._callbacks.push(callback);
  }


  setDefaultImage(url) {
    this.defaultImage = url;
  }
  //--------------------
  // GETTERS
  //--------------------
  get filterPanelData() {
    return this._filterPanelData

  }
  get searchResults() {
    return this._searchResultsData
  }

  get isOnSale() {
    return this._isOnSale;
  }

  get hasSales() {
    return this._hasSales;
  }

  get totalResults() {
    return this._totalResults;
  }

  //--------------------
  // "Private" methods ;-)
  //--------------------
  _initialize() {
    dispatcher.addListener(ADD_FILTER, (filter) => {
      this._addSearchFilter(filter);
    });

    dispatcher.addListener(REMOVE_FILTER, (filter) => {
      this._removeSearchFilter(filter);
    });

    dispatcher.addListener(FILTER_RESET, (filter) => {
      this._resetSearchFilters();
    });

    dispatcher.addListener(SALES_FILTER, (onSale) => {
      this._setOnSale(onSale);
    });
  }

  _parseOriginalData() {
    for (let i = 0; i < this._originalData.length; i++) {
      const node = this._originalData[i];
      const items = node.hits;

      if (items) {
        this._hasResults = true;

        let filterCategory = {
          label: node.category,
          shortcode: node.categoryId,
          totalResults: items.length,
          totalOnSale: 0,
          subCategories: []
        };

        let resultCategory = {
          label: node.category,
          shortcode: node.categoryId,
          landingUrl: node.categoryLandingUrl,
          totalResults: items.length,
          results: []
        }

        // Updating the global number of results
        this._originalTotalResults += items.length;

        // looping through all hits[] to create subCategories (filters) and searchResults
        for (let j = 0; j < items.length; j++) {
          const subNode = items[j];
          const isOnSale = (subNode.isOnSale.toLowerCase() === 'y') ? true : false;

          // -----------------------------
          // RESULTS PARSING
          // -----------------------------
          const result = {
            score: subNode.score,
            name: subNode.name,
            description: subNode.description,
            productUrl: subNode.productUrl,
            imageUrl: (subNode.imageUrl && subNode.imageUrl !== '') ? subNode.imageUrl : this.defaultImage,
            points: subNode.points,
            isOnSale,
            salePoints: subNode.salePoints,
            subCategories: []
          }

          if (isOnSale) {
            filterCategory.totalOnSale += 1;
            this._hasSales = true;
          }

          // -----------------------------
          // SUBCATEGORIES PARSING
          // -----------------------------

          // Grabbing the first subcategory
          if (subNode.categoryDetails[0].subCategory1
            && subNode.categoryDetails[0].subCategory1.length > 0) {
              this._addSubCategory(filterCategory, {
                label: subNode.categoryDetails[0].subCategory1,
                value: subNode.categoryDetails[0].subCategory1,
                isOnSale
            });

            result.subCategories.push(subNode.categoryDetails[0].subCategory1);
          }

          // Grabbing the second subcategory
          if (subNode.categoryDetails[0].subCategory2
            && subNode.categoryDetails[0].subCategory2.length > 0) {
              this._addSubCategory(filterCategory, {
                label: subNode.categoryDetails[0].subCategory2,
                value: subNode.categoryDetails[0].subCategory2,
                isOnSale
            });

            result.subCategories.push(subNode.categoryDetails[0].subCategory2);
          }

          // Adding the new
          resultCategory.results.push(result);
        }

        // UPDATING PARSED DATA ARRAYS
        filterCategory = this._checkExceptions(filterCategory)
        resultCategory = this._checkExceptions(resultCategory)

        this._filterPanelData.push(filterCategory);
        this._searchResultsData.push(resultCategory);

        this._totalResults = this._originalTotalResults;
      }
    }

    // We didn't get any results, so we need to 
    if(!this._hasResults) {
      dispatcher.dispatch(NO_RESULTS, this._originalData);
    
    } else {
      dispatcher.dispatch(HAS_RESULTS, this._originalData);
    }
  }

  // Adding a subCategory only if it doesn't exists yet
  _addSubCategory(category, subCategory) {
    const existingSubCategory = category.subCategories.find(({ label }) => label === subCategory.label);
    if (existingSubCategory === undefined) {
      category.subCategories.push({
        label: subCategory.label,
        value: subCategory.value,
        isOnSale: subCategory.isOnSale
      });
    } else if(!existingSubCategory.isOnSale && subCategory.isOnSale) {
      // If the subcategory exists but isOnSale differs, we update it
      existingSubCategory.isOnSale = true
    }

    return category;
  }


  // Here we filter search results based on selected filters
  _updateSearchResults() {
    // First, we retrieve the original JSOn and make a deep copy
    const data = JSON.parse(JSON.stringify(this._searchResultsData));

    // Reset these variables
    this._totalResults = 0;
    let hasSales = false;
    
    if(this._selectedFilters.length > 0) {
      // We have filters selected
      const updatedPanelData = data.filter( el => {
        const items = el.results.filter(subCat => {
          const res = subCat.subCategories.filter((cat => {
            return this._selectedFilters.some(label => {
              return label === cat;
            });
          }));

          const foundItems = (this._isOnSale) 
            ? (subCat.isOnSale && res.length > 0)
            : res.length > 0;
          
          return foundItems;
        });
  
        el.results = items;

        if(items.length > 0) {
          this._totalResults += items.length;
          if(items.find(item => item.isOnSale)) hasSales = true;
        }

        return items.length > 0;
      });
      
      dispatcher.dispatch(RESULTS_UPDATE, {
        results: updatedPanelData,
        hasSales: hasSales
      });

    } else {
      // No filters selected
      let updatedPanelData
      if(this._isOnSale) {
        // But sales is selected
        updatedPanelData = data.filter( el => {
          const items = el.results.filter(subCat => {
            return subCat.isOnSale
          });
    
          el.results = items;
          
          if(items.length > 0) {
            this._totalResults += items.length;
            hasSales = true;
          }

          return items.length > 0
        });

      } else {
        updatedPanelData = data;
        hasSales = this._hasSales;
        this._totalResults = this._originalTotalResults;
      }

      // No filters, so we just return the whole data and reset the total matches
      dispatcher.dispatch(RESULTS_UPDATE, {
        results: updatedPanelData,
        hasSales: hasSales
      });
    }
  }

  _addSearchFilter(value) {
    if (!this._selectedFilters.find((filter) => filter === value)) {
      this._selectedFilters.push(value);
      this._updateSearchResults();
    }
  }

  // Removing a filter after its deselection
  _removeSearchFilter(value) {
    this._selectedFilters = this._selectedFilters.filter((filter) => filter != value);
    this._updateSearchResults();
  }

  _setOnSale(onSale=false) {
    this._isOnSale = onSale;
    this._updateSearchResults();
  }

  _resetSearchFilters() {
    this._selectedFilters = [];
    this._isOnSale = false;
    this._totalResults = this._originalTotalResults;
    dispatcher.dispatch(RESULTS_UPDATE, {
      results: this._searchResultsData,
      hasSales: this._hasSales
    });
  }

  _checkExceptions(category) {
    // First we check if any exceptions exist in the config file
    if (!config.categories[category.shortcode] || !config.categories[category.shortcode].exceptions) {
      // If not, we return the object intact
      return category;
    }

    const exceptions = config.categories[category.shortcode].exceptions;

    if (exceptions.preLabel) {
      category.label = exceptions.preLabel + category.label;

    } else if (exceptions.preSubLabel) {

      if (category.subCategories) {
        category.subCategories.map(node => node.label = exceptions.preSubLabel.replace(/\[data\]/gi, node.label));  

      } else if (category.results) {
        category.results.map(node => node.label = exceptions.preSubLabel + node.label);
      }

    } else if (exceptions.forcedSubCategory) {
      if (category.subCategories) {
        category.subCategories.map(node => node.label = category.label);

      } else if (category.results) {
        category.results.map(node => node.subCategories[0] = category.label);
      }
    }
    return category
  }

}

export { ResultDataManager }
import 'core-js/stable/promise'
import 'core-js/modules/es.array.find';
import 'core-js/proposals/url'
import 'regenerator-runtime/runtime';
import 'whatwg-fetch'

import { navSearchInit } from './nav-search.js'
import { closestPolyfill, customEvent } from './utils'
import { SearchController } from './search/searchController'

// const mockdataUrl = 'http://www.mocky.io/v2/5e9ef0cd2d00007500cb789d?mocky-delay=100ms';
// const mockdataUrl = '../mockdata/page-results.json';

// To test no results
const noResults = new URLSearchParams(window.location.search).get('results');
const term = new URLSearchParams(window.location.search).get('term');
const isSynonym = new URLSearchParams(window.location.search).get('synonym');

let dataUrl;

(function() {
    closestPolyfill();
    customEvent();
    navSearchInit();

    const element = document.querySelector('.cs-search-page-container');

    if(element) {

      if(element.dataset.apiUrl && element.dataset.apiUrl !== '') {
        dataUrl = element.dataset.apiUrl;
      
      } else {
        
        // Loading mock data since we didn't receive the API URL
        if(noResults && noResults === 'no') {
          dataUrl = '../mockdata/no-results-search.json';
        } else if(term) {
          dataUrl = '../mockdata/term-results.json';
        } else if(isSynonym) {
          dataUrl = '../mockdata/synonym-results.json';
        } else {
          dataUrl = '../mockdata/page-results.json';
        }
      }
      const searchController = new SearchController(dataUrl);
      searchController.init();

    } else {
      console.log('no HTML container')
    }
})();




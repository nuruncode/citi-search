import { SearchResultsBlock } from './searchResultsBlock'
import dispatcher from '../events/dispatcher'
import { RESULTS_UPDATE } from '../events/events'
import config from '../search/config'

class SearchResults {
  constructor(containerElement) {
    if(containerElement) {
      this.containerElement = containerElement;
      this.containerElement.innerHTML = '';
      this.initialize();
    } else {
      console.log('SearchResults Error: containerElement is: ', containerElement)
    }
  }

  initialize() {
    // Initliazing variables
    this.data = [];
    this.blocks = [];
    this.uiElement = null;

    dispatcher.addListener(RESULTS_UPDATE, ({ results }) => {
      this.setData(results);
      this.reset();
    });
  }

  setData(resultsData) {
    this.data = resultsData;
  }

  reset() {
    this.blocks.forEach((block) => {
      block.destroy();
    })
    this.blocks = [];
    this.render();
  }

  // HTML rendering
  render() {
    this.containerElement.innerHTML = '';

    // Creating the <ul> element
    this.uiElement = document.createElement('ul');
    this.uiElement.setAttribute('id', 'cs-search-page-results');
    this.uiElement.classList.add('cs-search-page-results');
    this.containerElement.appendChild(this.uiElement);

    for (let i = 0; i < this.data.length; i++) {
      const node = this.data[i];

      // Creating the <li> element
      const li = document.createElement('li');
      li.setAttribute('id', `search-page-result-${i}`);
      li.classList.add('cs-search-page-result-list');
      li.classList.add('jsCsResultList');
      this.uiElement.appendChild(li);

      
      // Creating block instances
      const block = new SearchResultsBlock(li, node.shortcode, node.label, node.totalResults, node.results);
      block.render();

      this.blocks.push(block);
    }
  }
}

export { SearchResults }
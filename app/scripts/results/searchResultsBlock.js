import { SearchResultsItem } from './searchResultsItem'

const MOBILE = 'MOBILE';
const TABLET = 'TABLET';
const DESKTOP = 'DESKTOP';

const EXPAND_CLOSED = 'expandClosed';
const EXPAND_OPEN = 'expandOpen';
const EXPAND_FULL = 'expandFull';

const breakpoints = {
  mobile: 599,
  tablet: 1080
}

class SearchResultsBlock {
  constructor(containerElement, id, label, totalResults, data) {
    this.containerElement = containerElement;
    this.data = data;
    this.label = label;
    this.id = id;
    this.totalResults = totalResults;
    this.collapsedHeight = 0;
    this.cardLoad = 6;
    this.maxCardsPerRow = 3;
    
    this.initialize();
  }
  
  initialize() {
    this.expandedState = EXPAND_CLOSED;
    this.uiElement = null;
    this.listItems = [];
    
    this.updateBreakpoint();
    this.currentlyShownCards = this.maxCardsPerRow;

    this.resizeHandler = (event) => this.onResize();

    window.addEventListener('resize', this.resizeHandler);
  }

  get expandLabel() {
    return (this.expandedState === EXPAND_FULL) ? 'SEE LESS' : 'SEE MORE';
  }

  render() {
    // Creating the title <h2>
    const title = document.createElement('h2');
    title.appendChild(document.createTextNode(this.label)); 
    this.containerElement.appendChild(title);

    // Creating the <ul> element
    this.listContainer = document.createElement('ul');
    this.listContainer.setAttribute('id', `cs-search-page-result-container-ul-${this.id}`);
    this.listContainer.classList.add('cs-search-page-result-container-ul');
    this.containerElement.appendChild(this.listContainer);

    this.renderExpandButton();

    // Creating the subCategories
    this.renderList(this.listContainer);

    this.setVisibleCards();
  }

  renderExpandButton() {
    // Base button
    this.expandButton = document.createElement('button');
    this.expandButton.classList.add('cs-search-page-result-show-more-btn');
    this.expandButton.setAttribute('aria-expanded', 'false');
    
    // Label
    this.expandButtonLabel = document.createTextNode(this.expandLabel);
    this.expandButton.appendChild(this.expandButtonLabel);

    // Creating the <i> element
    this.arrowIcon = document.createElement('i');
    this.arrowIcon.classList.add('cs-arrow-icon-down');
    this.expandButton.appendChild(this.arrowIcon);
    
    this.expandButton.addEventListener('click', (event) => {
      this.setExpand();
      this.setVisibleCards();
    })
    
    this.containerElement.appendChild(this.expandButton);
  }

  renderList(ulElement) {
    if(!ulElement) {
      console.log('SearchResultBlock: renderList: missing rendering first');
      return 
    }

    for (let i = 0; i < this.data.length; i++) {
      const node = this.data[i];
      
      const li = document.createElement('li');
      li.classList.add('cs-search-page-result-item');

      ulElement.appendChild(li);
      
      const item = new SearchResultsItem(li, this.id, i, node);
      item.render();
      
      this.listItems.push(li);
    }
    
    this.setVisibleCards();
  }

  onResize(event) {
    this.updateBreakpoint();

    if(this.expandedState === EXPAND_CLOSED) {
      this.currentlyShownCards = this.maxCardsPerRow;
    }
    
    this.setVisibleCards();
  }

  updateBreakpoint(){
    const w = window.innerWidth;
    
    if(w < breakpoints.tablet && w > breakpoints.mobile) {
      this.breakpoint = TABLET;
      this.maxCardsPerRow = 2;
    
    } else if(w <= breakpoints.mobile) {
      this.breakpoint = MOBILE;
      this.maxCardsPerRow = 1;
    } else {
      this.breakpoint = DESKTOP;
      this.maxCardsPerRow = 3;
    }
  }

  setExpand() {
    if(this.listItems.length === this.currentlyShownCards) {
      // we are collapsing
      this.currentlyShownCards = this.maxCardsPerRow;
      this.expandedState = EXPAND_CLOSED;
      
      window.scrollTo({
        top: this.listContainer.parentElement.offsetTop,
        left: 0,
        behavior: 'smooth'
      });
      
    } else {
      // we are adding 6 cards
      this.expandedState = EXPAND_OPEN;
      this.currentlyShownCards = Math.min(this.listItems.length, this.currentlyShownCards + this.cardLoad);
    }
    
    if(this.listItems.length - this.currentlyShownCards < 2 
      && this.currentlyShownCards !== this.maxCardsPerRow) {
      
        // Only 1 left so we are showing them all
        this.currentlyShownCards = this.listItems.length;
      }
      
    if(this.currentlyShownCards === this.listItems.length) {
      // We are showing them all
      this.expandedState = EXPAND_FULL;
    }
      
    
    // Update the button label
    this.expandButtonLabel.textContent = this.expandLabel;

    if (this.arrowIcon.classList.contains('open')) {
      this.arrowIcon.classList.remove('open');
    } else {
      this.arrowIcon.classList.add('open');
    }
  }

  setVisibleCards() {
    for (let i = 0; i < this.listItems.length; i++) {
      const item = this.listItems[i];
      item.style.display = (i < this.currentlyShownCards) ? 'block' : 'none';
    }

    if(this.listItems.length <= this.currentlyShownCards && this.expandedState === EXPAND_CLOSED) {
      this.expandButton.style.display = 'none';
    
    } else if(this.currentlyShownCards <= this.maxCardsPerRow && this.expandedState !== EXPAND_CLOSED) {
      this.expandButton.style.display = 'none';
      
    } else {
      this.expandButton.style.display = 'block';
    }
  }

  destroy() {
    window.removeEventListener('resize', this.resizeHandler);
  }

}

export { SearchResultsBlock }

import config from '../search/config'
import { formatNumberThousand } from '../utils'

class SearchResultsItem {
  constructor(containerElement, parentId, id, data) {
    this.containerElement = containerElement;
    this.id = id;
    this.data = data;
    this.parentId = parentId;
    this.config = config.categories[this.parentId];
    
    this.initialize();
  }
  
  initialize() {
    this.element = null;

    this.exceptions = this.checkExceptions();
  }

  render() {
    const ul = this.containerElement;
    ul.innerHTML = this.getNodeMarkup();
  }

  checkExceptions() {
    // First we check if any exceptions exist in the config file
    if(!config.categories[this.parentId] || !config.categories[this.parentId].exceptions) {
      // If not, we return the object intact
      return null;
    }

    return config.categories[this.parentId].exceptions;
  }

  getCtaLabel() {
    let label = (this.config)
      ? this.config.searchCtaLabel.substr(0)
      : ''

    label = label.replace(/\[name\]/gi, this.data.name);
    label = label.replace(/\[subcategory\]/gi, this.data.name.toLowerCase());
    return label;
  }

  truncate(value, maxChars=100) {
    if(value.length > maxChars) {
      let trimmedValue = value.substr(0, maxChars);
      trimmedValue  = trimmedValue.substr(0, Math.min(trimmedValue.length, trimmedValue.lastIndexOf(' ')));
      
      return `${trimmedValue}...`

    } else {
      return value
    }
  }


  getNodeMarkup() {
    /*
      score: subNode.score,
      name: subNode.name,
      description: subNode.description,
      productUrl: subNode.productUrl,
      imageUrl: subNode.imageUrl,
      points: subNode.points,
      isOnSale: (subNode.isOnSale.toLowerCase() === 'y') ? true : false,
      salePoints: subNode.salePoints,

    */

    const giftImageClass = (this.parentId === 'GIFTCARDS') ? 'cs-image-gift-card' : '';

    const img = (this.exceptions && this.exceptions.hideImage) 
      ? ''
      : `
      <div class="cs-search-page-result-image-container ${giftImageClass}">
        <img src="${this.data.imageUrl}" alt="${this.data.name}" />
      </div>
      `;

    const saleNode = `
    <p class="cs-search-page-result-copy text-alert">${formatNumberThousand(this.data.salePoints)} points
      <span class="cs-search-page-result-sale-icon">SALE</span>
    </p>
    `

    const copyNode = (this.exceptions && this.exceptions.showDescription === false) 
      ? ''
      : `<p class="cs-search-page-result-copy">
          ${this.data.description}
        </p>`

    const pointsNode = `
    <p class="cs-search-page-result-copy">Starting at
      <span class="${(this.data.isOnSale) ? 'text-strikeout' : ''}">${formatNumberThousand(this.data.points)} points</span>
      <span class="visuallyhidden">Original points</span>
    </p>`

    const cta = (this.config && this.config.searchCtaLabelType === 'text') 
      ? `<div class="cs-search-page-result-link">
          <a href="${this.data.productUrl}" aria-label="${this.getCtaLabel()} - ${this.data.name}">${this.getCtaLabel()}&nbsp;<span aria-hidden="true">></span></a>
        </div>`
      : `<div class="cs-search-page-result-btn">
          <a class="btn" href="${this.data.productUrl}" aria-label="${this.getCtaLabel()} - ${this.data.name}">${this.getCtaLabel()}</a>
        </div>`

    const title = (this.exceptions && this.exceptions.truncate)
      ? `<span aria-label="Sentence Fragment:"></span>${this.truncate(this.data.name)}`
      : this.data.name


    return `
      <div class="cs-search-page-result-item-container">
        ${(this.data.imageUrl !== '') ? img : ''}
        <div class="cs-search-page-result-wrapper">
          <div class="cs-search-page-result-details">
            <h3 class="cs-search-page-result-detail-title">${title}</h3>
            
            ${(this.data.points !== '') ? pointsNode : copyNode}
            ${(this.data.isOnSale) ? saleNode : ''}
          </div>
          ${cta}
        </div>
      </div>
    `
  }
}


export { SearchResultsItem }

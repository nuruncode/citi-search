import dispatcher from '../events/dispatcher'
import { SALES_FILTER } from '../events/events'
class FilterItem {
  constructor(containerElement, parentId, id, { label, value, isOnSale }) {
    this.containerElement = containerElement;
    this.id = id;
    this.parentId = parentId;
    this.label = label;
    this.value = value;
    this.isOnSale = isOnSale;
    
    this.initialize();
  }
  
  initialize() {
    this.selected = false;
    this.element = null;
    this.callback = null;
    this.aElement = null;

    dispatcher.addListener(SALES_FILTER, (showOnSale) => {
      this.toggleOnSale(showOnSale);
    })
  }

  toggleSelected() {
    this.selected = !this.selected;
    
    if(this.selected) {
      this.aElement.classList.add('selected');
      this.aElement.setAttribute('aria-pressed', 'true');
    } else {
      this.aElement.classList.remove('selected');
      this.aElement.setAttribute('aria-pressed', 'false');
    }
  }

  toggleOnSale(showOnSale) {
    if(showOnSale && !this.isOnSale) {
      this.aElement.style.display = 'none';
    } else {
      this.aElement.style.display = 'flex';
    }
  }

  setCallback(callback) {
    this.callback = callback;
  }

  render() {
    const ul = this.containerElement;

    // Creating the <li> element
    const li = document.createElement('li');
    li.setAttribute('id', `filter-block-list-item-${this.parentId}-${this.id}`);
    
    // Creating the <a> element
    this.aElement = document.createElement('a');

    // Creating the <i> element
    const crossIcon = document.createElement('i');
    crossIcon.classList.add('cs-input-close-icon');
    this.aElement.appendChild(crossIcon);  // prepend is not supported on IE11

    const text = document.createTextNode(this.label); 
    this.aElement.appendChild(text); 

    this.aElement.setAttribute('data-list-id', this.id);
    this.aElement.setAttribute('aria-pressed', 'false');
    this.aElement.setAttribute('role', 'button');
    this.aElement.title = this.label;
    this.aElement.href = '#';  
    this.aElement.addEventListener('click', (event) => {
      event.preventDefault();
      this.onSelect(event);
    });

    // Adding children
    li.appendChild(this.aElement);
    ul.appendChild(li);
  }

  reset() {
    this.selected = false;
    this.aElement.style.display = 'flex';
    this.aElement.classList.remove('selected');
  }

  // Events
  onSelect(event) {
    this.toggleSelected();
    
    if(this.callback !== null) this.callback(this.id, this.value, this.selected);
  }

}

export { FilterItem }

import { FiltersBlock } from './filtersBlock'
import dispatcher from '../events/dispatcher'
import { ADD_FILTER, REMOVE_FILTER, SALES_FILTER, FILTER_RESET, RESULTS_UPDATE } from '../events/events'
import config from '../search/config'
import initFiltersExpand from '../search/search-filters-expand'

// TODO:
// - Add Sales & Promotion block


class FiltersPanel {
  constructor(containerElement) {
    if(containerElement) {
      this.containerElement = containerElement;
      this.containerElement.innerHTML = '';
      this.initialize();
    } else {
      console.log('FiltersPanel Error: containerElement is: ', containerElement)
    }
  }

  initialize() {
    // Initliazing variables
    this.blocks = [];
    this.categories = [];
    this.selectedFilters = [];
    this.filteredData = [];
    this.uiElement = null;
    this.isOnSale = false;
    this.displayOnSale = false;

    dispatcher.addListener(RESULTS_UPDATE, ({ results, hasSales }) => {
      this.checkboxSales.disabled = !hasSales;
      this.checkboxSales.setAttribute('aria-disabled', !hasSales);
    })
  }

  setData(categoriesData, isOnSale) {
    this.categories = categoriesData;
    this.isOnSale = isOnSale;
  }

  // Addiing a filter after its selection
  addFilter(value) {
    if(!this.selectedFilters.find((filter) => filter === value)) {
      this.selectedFilters.push(value);
    }
  }

  // Removing a filter after its deselection
  removeFilter(value) {
    this.selectedFilters = this.selectedFilters.filter((filter) => filter != value);
  }

  // updating views based on 
  updateViews(selectedBlockId) {
    this.blocks.forEach((block) => {
      if(selectedBlockId === undefined || selectedBlockId === null) {
        if(!this.displayOnSale) block.show();
        
      } else if(block.id !== selectedBlockId) {
        block.hide();
      }
    })
  }
  
  // HTML rendering
  render() {
    this.containerElement.innerHTML = '';

    // Creating wrapper
    const titleWrapper = document.createElement('div');
    titleWrapper.classList.add('cs-filter-panel-title-wrapper');
    this.containerElement.appendChild(titleWrapper);

    // Creating the title bar element
    const titleElement = document.createElement('h2');
    titleElement.setAttribute('id', 'cs-filter-panel-title');
    titleElement.appendChild(document.createTextNode(config.filters.title)); // Adding text
    titleElement.classList.add('cs-filter-panel-title');
    titleWrapper.appendChild(titleElement);
    
    // Creating the clear button element
    const clearButton = document.createElement('span');
    clearButton.setAttribute('id', 'cs-filter-panel-clear');
    clearButton.classList.add('cs-filter-panel-clear');

    // Clear Button A tag
    const aTag = document.createElement('a');
    aTag.appendChild(document.createTextNode(config.filters.clearLabel)); // Adding text
    aTag.title = config.filters.clearLabel;
    aTag.href = '#';  
    aTag.addEventListener('click', (event) => {
      event.preventDefault();
      this.onClear(event);
    });
    clearButton.appendChild(aTag);
    titleWrapper.appendChild(clearButton);
    
    // Creating Sales & Promotions input if applicable
    this.saleElement = document.createElement('div');
    this.saleElement.setAttribute('id', 'cs-filter-panel-sales');
    this.saleElement.classList.add('cs-filter-panel-sales');
    this.containerElement.appendChild(this.saleElement);
    
    // Checkbox
    this.checkboxSales = document.createElement('input');
    this.checkboxSales.setAttribute('id', 'cs-filter-panel-sales-input');
    this.checkboxSales.setAttribute('name', 'cs-filter-panel-sales-input');
    this.checkboxSales.setAttribute('type', 'checkbox');
    this.checkboxSales.setAttribute('aria-checked', 'false');
    this.checkboxSales.setAttribute('aria-disabled', !this.isOnSale);
    this.checkboxSales.disabled = !this.isOnSale;
    this.checkboxSales.classList.add('cs-filter-panel-sales-input');
    this.checkboxSales.addEventListener('change', (event) => {
      this.onDisplaySales(event);
    })
    this.saleElement.appendChild(this.checkboxSales);

    // Checkbox Label
    const label = document.createElement('label');
    label.appendChild(document.createTextNode(config.filters.salesLabel)); // Adding text
    label.setAttribute('id', 'cs-filter-panel-sales-input-label');
    label.setAttribute('for', 'cs-filter-panel-sales-input');
    label.classList.add('cs-filter-panel-sales-input-label');
    this.saleElement.appendChild(label);

    // Creating the <ul> element
    this.uiElement = document.createElement('ul');
    this.uiElement.setAttribute('id', 'cs-filter-panel-wrapper');
    this.uiElement.classList.add('cs-filter-panel-wrapper');
    this.containerElement.appendChild(this.uiElement);

    for (let i = 0; i < this.categories.length; i++) {
      // Creating the <li> element
      const li = document.createElement('li');
      li.setAttribute('id', `filter-block-li-${i}`);
      li.classList.add(`filter-block-li-${i}`);
      this.uiElement.appendChild(li);

      const node = this.categories[i];
      const exceptions = this.checkExceptions(node);
      
      // Creating block instances
      const subCategories = (exceptions && exceptions.noSubCategories) ? [] : node.subCategories;
      const block = new FiltersBlock(li, node.shortcode, node.label, node.totalResults, node.totalOnSale, subCategories);
      
      block.setCallback((id, value, selected) => {
        this.onItemSelect(id, value, selected);
      });

      block.render();

      this.blocks.push(block);
    }

    initFiltersExpand();
  }

  checkExceptions(category) {
    // First we check if any exceptions exist in the config file
    if(!config.categories[category.shortcode] || !config.categories[category.shortcode].exceptions) {
      // If not, we return the object intact
      return null;
    }

    return config.categories[category.shortcode].exceptions
  }

  onItemSelect(id, value, selected) {
    if(!selected) {
      this.removeFilter(value);
      dispatcher.dispatch(REMOVE_FILTER, value);
    } else {
      this.addFilter(value);
      dispatcher.dispatch(ADD_FILTER, value);
    }
    
    const blockId = (this.selectedFilters.length > 0) ? id : null;
    this.updateViews(blockId);
  }
  
  onClear() {
    this.blocks.forEach(block => {
      block.reset();
    });

    this.checkboxSales.checked = false;
    this.displayOnSale = false;

    this.selectedFilters = [];
    this.updateViews();
    dispatcher.dispatch(FILTER_RESET);
  }

  onDisplaySales(event) {
    this.displayOnSale = !this.displayOnSale;

    this.checkboxSales.setAttribute('aria-checked', this.displayOnSale);

    this.blocks.forEach(block => {
      if(this.displayOnSale) {
        block.displayOnSaleTotal();
        if(!block.hasSales) block.hide();
        
      } else {
        block.displayResultsTotal();
        if(this.selectedFilters.length < 1) block.show();
      }
    });

    dispatcher.dispatch(SALES_FILTER, this.displayOnSale);
  }

}

export { FiltersPanel }

import { FilterItem } from './filterItem'
import config from '../search/config'
import dispatcher from '../events/dispatcher'
import { SALES_FILTER } from '../events/events'

class FiltersBlock {
  constructor(containerElement, id, label, totalResults, totalOnSale, filterList) {
    this.containerElement = containerElement;
    this.label = label;
    this.id = id;
    this.totalResults = totalResults;
    this.totalOnSale = totalOnSale;
    this.nodes = filterList;
    this.visible = true;
    
    this.initialize();
  }
  
  initialize() {
    this.expanded = false;
    this.selected = false; // use only if no subsections
    this.uiElement = null;
    this.callback = null;
    this.listItems = [];
  }

  get hasSales() {
    return this.totalOnSale > 1
  }
  
  setCallback(callback) {
    this.callback = callback;
  }
  
  toggleExpand() {
    this.expanded = !this.expanded;
  }

  toggleSelected() {
    this.selected = !this.selected;
    
    if(this.selected) {
      this.title.classList.add('selected');
      // this.title.setAttribute('aria-selected', 'true');
    } else {
      this.title.classList.remove('selected');
      // this.title.setAttribute('aria-selected', 'false');
    }
  }

  displayOnSaleTotal() {
    this.titleLabel.textContent = `${this.label} (${this.totalOnSale})`;
  }

  displayResultsTotal() {
    this.titleLabel.textContent = `${this.label} (${this.totalResults})`;
  }

  hide() {
    this.uiElement.style.display = 'none';
    this.uiElement.classList.add('hidden');
    this.visible = false;
  }
  
  show() {
    this.uiElement.style.display = 'block';
    this.uiElement.classList.remove('hidden');
    this.visible = true;
  }
  
  render() {
    const exceptions = this.checkExceptions();

    // Creating the wrapper <div>
    this.uiElement = document.createElement('div');
    this.uiElement.classList.add('cs-filter-block');
    this.containerElement.appendChild(this.uiElement);
    
    this.title = document.createElement('button');
    this.title.setAttribute('aria-expanded', 'false');
    this.title.classList.add('cs-filter-block-title');
    this.uiElement.appendChild(this.title);
    
    // We are setting this only if there are no children
    if(exceptions && exceptions.noSubCategories) {
      this.title.classList.add('clickable');
      // this.title.setAttribute('aria-selected', 'false');
      
      // Creating the <i> element
      const crossIcon = document.createElement('i');
      crossIcon.classList.add('cs-input-close-icon');
      this.title.appendChild(crossIcon); // prepend is not supported on IE11
      
      this.title.addEventListener('click', (event) => {
        this.toggleSelected();
        this.onSelect(this.id, this.label, this.selected);
      })
    }
    
    // Creating the title <h3>
    this.titleLabel = document.createTextNode(`${this.label} (${this.totalResults})`);
    this.title.appendChild(this.titleLabel); 


    // Creating the <i> element
    const arrowIcon = document.createElement('i');
    arrowIcon.classList.add('cs-arrow-icon-down');
    this.title.appendChild(arrowIcon);


    // Creating the <ul> element
    const ul= document.createElement('ul');
    ul.setAttribute('id', `cs-filter-block-list-${this.id}`);
    ul.classList.add('cs-filter-block-list');
    this.uiElement.appendChild(ul);

    // Creating the subCategories
    if(this.nodes && this.nodes.length > 0) this.renderList(ul);
  }

  renderList(ulElement) {
    if(!ulElement) {
      console.log('FilterBlock: renderList: missing rendering first');
      return 
    }

    for (let i = 0; i < this.nodes.length; i++) {
      const node = this.nodes[i];

      const item = new FilterItem(ulElement, this.id, i, node);
      item.setCallback((id, value, selected) => {
        this.onSelect(id, value, selected);
      });
      item.render();
      this.listItems.push(item);
    }
  }

  checkExceptions() {
    // First we check if any exceptions exist in the config file
    if(!config.categories[this.id] || !config.categories[this.id].exceptions) {
      // If not, we return the object intact
      return null;
    }

    return config.categories[this.id].exceptions;
  }

  reset() {
    this.selected = false;
    this.expanded = false;

    this.displayResultsTotal();

    this.listItems.forEach(item => {
      item.reset();
    })
  }

  // Events
  onSelect(id, value, selected) {
   if(this.callback !== null) this.callback(this.id, value, selected);
  }
}

export { FiltersBlock }

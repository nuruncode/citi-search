export const jsonFetch = async (url, query) => {
    // return data from fetch url.
    // query is from the input value or you can bring it from the url.
    // Api endpoint from epsilon needs to be passed in the url parameter:
    // For dropdown: https://tyqa.epsilon.com/searchSuggestions.htm?src=TYUSENG&keyword=
    // For Results Page: https://tyqa.epsilon.com/search.htm?src=TYUSENG&keyword=

    // Data needs to be collected in an async function:
    // let getDataInThisFunction = async () => {const data = await jsonFetch(url, query , mockdataUrl)}
    try {

        let source = {};

        if (url && query) {
            source = await fetch(`${url}${query}`);
        } else if (url) {
            source = await fetch(url);
        }

        // Return fetched json data.
        return await source.json();

    } catch (err) {
        console.log(err)
    }
};

//Utils function debounce
export const debounce = (func, delay) => {
    let inDebounce;
    return function () {
        const context = this;
        const args = arguments;
        clearTimeout(inDebounce);
        inDebounce = setTimeout(function () {
            return func.apply(context, args);
        }, delay);
    };
};

export const triggerEvent = (el, type) => {
    if ('createEvent' in document) {
        // modern browsers, IE9+
        const event = document.createEvent('HTMLEvents');
        event.initEvent(type, false, true);
        el.dispatchEvent(event);
    } else {
        // IE 8
        const event = document.createEventObject();
        event.eventType = type;
        el.fireEvent('on' + event.eventType, event);
    }
}

export const closestPolyfill = () => {
    if (!Element.prototype.matches)
        Element.prototype.matches = Element.prototype.msMatchesSelector ||
        Element.prototype.webkitMatchesSelector;

    if (!Element.prototype.closest)
        Element.prototype.closest = function (s) {
            var el = this;
            if (!document.documentElement.contains(el)) return null;
            do {
                if (el.matches(s)) return el;
                el = el.parentElement || el.parentNode;
            } while (el !== null && el.nodeType == 1);
            return null;
        };
}


export const getInnerHtml = (node, label) => {
    let result = node ? node.innerHTML : '';

    if (result !== '' && label) {
        result = result.replace('${label}', label)
    }

    return result;
}

export const fetchWithTimeout = (url, timeout = 5000) => {
    return new Promise((resolve, reject) => {
        // Set timeout timer in millisecond
        let timer = setTimeout(
            () => reject(new Error('Request timed out')),
            timeout
        );

        fetch(url).then(
            response => resolve(response.json()),
            err => reject(err)
        ).finally(() => clearTimeout(timer));
    })
}

export const hasClass = (element, className) => {
    return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
}

export const isElement = el => el instanceof Element;

export const removeElement = (element) => {
    // Removes an element from the document
    if (isElement(element)) {
        const parentNode = element.parentNode;
        if (parentNode) {
            element.parentNode.removeChild(element);
        }
    }
}

export const customEvent = () => {

    if ( typeof window.CustomEvent === 'function' ) return false;
  
    function CustomEvent ( event, params ) {
      params = params || { bubbles: false, cancelable: false, detail: null };
      var evt = document.createEvent( 'CustomEvent' );
      evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
      return evt;
     }
  
    window.CustomEvent = CustomEvent;
};

export const formatNumberThousand = (value) => {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
/*eslint-env jquery*/

const initFiltersExpand = () => {
    $(function () {
        let jsCsExpandUl = $('.cs-filter-block-title:not(.clickable)');
        let jsCsFilterBtn = $('.cs-search-page-btn-filter');
        let jsCsFilterResultBtn = $('.cs-search-page-btn-filter-result');
        let jsCsFilterCrossBtn = $('#jsCsCloseIconFilter');
        let jsCsFilterWrapper = $('.cs-search-page-filter-wrapper');
        let jsCsResultContainer = $('.cs-search-page-result-container');
        let jsCsFooterTabIndex = $('footer a');
        let jsCsHeaderTabIndex = $('#header-mobile a');

        //Button expand
        jsCsExpandUl.on('click', function (event) {
            let thisButton = $(event.currentTarget);
            if (thisButton.length === 0) {
                return;
            }
            let thisButtonParent = thisButton.parent();
            let listItem = thisButtonParent.find('.cs-filter-block-list');
            if (listItem.length === 0) {
                return;
            }
            let icon = thisButtonParent.find('.cs-arrow-icon-down');

            listItem.animate({
                height: "toggle"
            }, 100, function () {
                if (listItem.filter(':visible').length === 1) {
                    thisButton.attr('aria-expanded', 'true');
                } else {
                    thisButton.attr('aria-expanded', 'false');
                }
            });

            if (icon.hasClass('open')) {
                icon.removeClass('open');
            } else {
                icon.addClass('open');
            }
        });

        //Button filter
        jsCsFilterBtn.on('click', toggleFilterBtn);

        //Button filterResult
        jsCsFilterResultBtn.on('click', function () {
            toggleFilterBtn();
            removeTabIndex();
        });

        //Button filter crossBtn
        jsCsFilterCrossBtn.on('click', function () {
            toggleFilterBtn();
            removeTabIndex();
        });

        function toggleFilterBtn() {
            jsCsFilterWrapper.toggleClass('jsCsFilterDisplayBlock');
            jsCsResultContainer.toggleClass('jsCsFilterDisplayBlock');
            addTabIndex();

            if (jsCsFilterWrapper.hasClass('jsCsFilterDisplayBlock')) {
                $(window).resize(closeOnResize);
            } else {
                $(window).off('resize', closeOnResize);
            }
        }

        function closeOnResize() {
            jsCsFilterWrapper.removeClass('jsCsFilterDisplayBlock');
            jsCsResultContainer.removeClass('jsCsFilterDisplayBlock');
            removeTabIndex();

            $(window).off('resize', closeOnResize);
        }

        function addTabIndex() {
            jsCsFooterTabIndex.attr('tabIndex', '-1');
            jsCsHeaderTabIndex.attr('tabIndex', '-1');
            jsCsFooterTabIndex.attr('aria-hidden', 'true');
            jsCsHeaderTabIndex.attr('aria-hidden', 'true');
        }

        function removeTabIndex() {
            jsCsFooterTabIndex.removeAttr('tabIndex');
            jsCsHeaderTabIndex.removeAttr('tabIndex');
            jsCsFooterTabIndex.removeAttr('aria-hidden');
            jsCsHeaderTabIndex.removeAttr('aria-hidden');
            jsCsFilterBtn.focus();
        }
    });
};

export default initFiltersExpand;

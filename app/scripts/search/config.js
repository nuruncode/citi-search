export default {
  filters : {
    // title: 'RESULTS BY SECTION',
    // clearLabel: 'Clear',
    // salesLabel: 'Sales & Promotions'
  },
  // noResults: {
  //   title: 'Sorry, there are no matches for "[query]".',
  //   subtitle: 'Try searching for other keywords related to your search.'
  // },
  // noKeyword: {
  //   title: 'Sorry, there are no matches for " ".',
  //   subtitle: 'Try searching for other keywords related to your search.'
  // },
  categories: {
    SWP: {
      searchCtaLabel: '',
      searchCtaLabelType: 'button',
      exceptions: {
        preSubLabel: 'Use Points for [data]'
      }
    },
    GIFTCARDS: {
      searchCtaLabel: '',
      searchCtaLabelType: 'button'

    },
    TRAVEL: {
      searchCtaLabel: '',
      searchCtaLabelType: 'text',
      exceptions: {
        noSubCategories: true,
        forcedSubCategory: true
      }
    },
    MWTR: {
      searchCtaLabel: '',
      searchCtaLabelType: 'button',
    },
    FAQ: {
      searchCtaLabel: '',
      searchCtaLabelType: 'text',
      exceptions: {
        showDescription: false,
        truncate: true,
        showButton: false,
        hideImage: true
      }
    }
  }
}

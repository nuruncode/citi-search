import { FiltersPanel } from '../filters/filtersPanel'
import { SearchResults } from '../results/searchResults'
import { jsonFetch } from '../utils'
import { ResultDataManager } from '../data/resultDataManager'
import dispatcher from '../events/dispatcher'
import { RESULTS_UPDATE, NO_RESULTS, HAS_RESULTS } from '../events/events.js'
import config from './config'

class SearchController {

  constructor(dataUrl) {
    this.dataUrl = dataUrl;
    this.data;
    this.suggestedTerm;
    this.isSynonym;
    this.panel;
    this.results;
    this.dataManager;
    this.totalResultElement;
    this.searchTitle;
    this.keyword;
    this.panelView;
    this.searchResultsDefaultImage;
  }


  async init() {
    if(!this.checkKeyword()) {
      this.keyword = ' ';
      this.showNoResults(null);
      return;
    }

    dispatcher.addListener(NO_RESULTS, (data) => {
      this.showNoResults(data);
    })

    dispatcher.addListener(HAS_RESULTS, () => {
      this.initSearchResultsView();
    })

    this.loader = document.querySelector('.cs-search-page-heading-loading');
    
    this.data = await jsonFetch(this.dataUrl, null);

    if(this.loader) {
      this.loader.style.display = 'none';
    }
    
    // Checking if we received a suggested term
    if(this.data.suggestedTerm && this.data.suggestedTerm !== '') {
      this.suggestedTerm = this.data.suggestedTerm;
    }

    // Checking if we received a suggested term
    if(this.data.searchResultsDefaultImage && this.data.searchResultsDefaultImage !== '') {
      this.searchResultsDefaultImage = this.data.searchResultsDefaultImage;
    }

    // Checking if we received synonyms
    if(this.data.isSynonyms && this.data.isSynonyms.toUpperCase() !== 'N') {
      this.isSynonym = this.data.isSynonyms;
    }

    // Initializing the DataManager
    this.dataManager = new ResultDataManager();
    if(this.searchResultsDefaultImage) this.dataManager.setDefaultImage(this.searchResultsDefaultImage);
    this.dataManager.setOriginalData(this.data.results);
  }

  initSearchResultsView() {
    // We update the search title with the search term
    this.searchTitle = document.querySelector('.cs-search-page-heading-title');

    if(this.searchTitle) {
      this.searchTitle.textContent = this.searchTitle.dataset.text.replace(/\[data\]/gi, this.keyword);
    }
    
    // We set the total number of result from the search
    this.totalResultElement = document.querySelector('.cs-search-page-heading-total-result');
    if(this.totalResultElement) {
      this.totalResultElement.textContent = this.totalResultElement.dataset.text.replace(/\[data\]/gi, this.dataManager.totalResults);
    }
    
    // If we received a suggested term, we set it otherwise we hide the HTML element
    this.suggestedTermElement = document.querySelector('.cs-search-page-heading-did-you-mean');
    if(this.suggestedTermElement) {
      if(this.suggestedTerm) {
        const url = window.location.href.split('?')[0];

        const aTag = `<a href="${url}?keyword=${this.suggestedTerm}">${this.suggestedTerm}</a>`
        const text = this.suggestedTermElement.dataset.text.replace(/\[data\]/gi, aTag);

        this.suggestedTermElement.style.display = 'block';
        this.suggestedTermElement.innerHTML = text;
      
      } else {
        this.suggestedTermElement.style.display = 'none';
      }
    } else {
      console.timeLog('CS - ERROR: No container for suggested term header is present')
    }

    // If we received a suggested term, we set it otherwise we hide the HTML element
    this.synonymsElement = document.querySelector('.cs-search-page-heading-synonyms');
    if(this.synonymsElement) {
      if(this.isSynonym) {
        this.synonymsElement.style.display = 'block';
      
      } else {
        this.synonymsElement.style.display = 'none';
      }
    } else {
      console.timeLog('CS - ERROR: No container for synonyms header is present')
    }

    dispatcher.addListener(RESULTS_UPDATE, () => {
      this.onSearchUpdate()
    });

    this.initSearchPanel();
    this.initSearchResults();
  }

  initSearchPanel() {
    const filterPanelElement = document.querySelector('.cs-search-page-filter');

    config.filters = {
      title: filterPanelElement.dataset.title,
      clearLabel: filterPanelElement.dataset.clearLabel,
      salesLabel: filterPanelElement.dataset.salesLabel
    }
    
    this.panelView = new FiltersPanel(filterPanelElement)
    this.panelView.setData(this.dataManager.filterPanelData, this.dataManager.hasSales);
    this.panelView.render();
  }

  initSearchResults() {
    const searchResultsElement = document.querySelector('.cs-search-page-result-container');

    const categories = config.categories;

    // Retrieving data-attributes from HTML Element and updating the config
    categories.SWP.searchCtaLabel = searchResultsElement.dataset.swpCtaLabel,
    categories.SWP.exceptions.preSubLabel = searchResultsElement.dataset.swpCtaLabel
    
    categories.GIFTCARDS.searchCtaLabel = searchResultsElement.dataset.giftcardsCtaLabel,
    
    categories.TRAVEL.searchCtaLabel = searchResultsElement.dataset.travelCtaLabel,
    
    categories.MWTR.searchCtaLabel = searchResultsElement.dataset.mwtrCtaLabel,
    categories.FAQ.searchCtaLabel = searchResultsElement.dataset.faqCtaLabel,
    
    this.resultsView = new SearchResults(searchResultsElement)
    this.resultsView.setData(this.dataManager.searchResults);
    this.resultsView.render();
  }

  onSearchUpdate() {
    if(this.totalResultElement) {
      this.totalResultElement.textContent = `${this.dataManager.totalResults} RESULTS`
    }
  }

  // Gating function to make sure we received a keyword
  checkKeyword() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    this.keyword = urlParams.get('keyword');

    return (this.keyword && this.keyword.length > 0);
  }

  showNoResults(data) {
    const resultsContainerElement = document.querySelector('.cs-search-page-container');
    resultsContainerElement.innerHTML = '';
    resultsContainerElement.style.display = 'none';

    const rootElement = document.querySelector('.cs-no-result-page-container');
    rootElement.innerHTML = '';
    


    if(rootElement) {
      // Header
      const headerElement = document.createElement('div');
      headerElement.classList.add('cs-no-result-page-heading-container');
      rootElement.appendChild(headerElement);

      const titleElement = document.createElement('h1');
      titleElement.classList.add('cs-no-result-page-heading-container-title');
      titleElement.innerHTML = rootElement.dataset.title.replace(/\[data\]/gi, this.keyword);
      // titleElement.appendChild();
      headerElement.appendChild(titleElement);
      
      const subTitleElement = document.createElement('p');
      subTitleElement.classList.add('cs-no-result-page-heading-container-subtitle');
      subTitleElement.appendChild(document.createTextNode(rootElement.dataset.subtitle));
      headerElement.appendChild(subTitleElement);

      if(data) {
        const listWrapperElement = document.createElement('div');
        listWrapperElement.classList.add('cs-no-result-page-link-wrapper');
        rootElement.appendChild(listWrapperElement);
        
        const listItemElement = document.createElement('div');
        listItemElement.classList.add('cs-no-result-page-link-wrapper-item');
        listWrapperElement.appendChild(listItemElement);
        
        const listUlElement = document.createElement('ul');
        listItemElement.appendChild(listUlElement);
        
        for (let i = 0; i < data.length; i++) {
          const item = data[i];
          
          const listElement = document.createElement('li');
          
          const link = document.createElement('a');
          link.href = item.categoryLandingUrl;
          link.appendChild(document.createTextNode(item.category));

          listElement.appendChild(link);
          listUlElement.appendChild(listElement);
        }
      }
    }
  }
}

export { SearchController }
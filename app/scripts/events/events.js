// Filter events
const ADD_FILTER = 'AddFilterEvent';
const REMOVE_FILTER = 'RemoveFilterEvent';
const SALES_FILTER = 'SalesilterEvent';
const FILTER_SELECTED = 'FilterSelectedEvent';
const FILTER_RESET = 'FilterResetEvent';


// Content events
const RESULTS_UPDATE = 'ContentUpdateEvent';
const FILTERS_UPDATE = 'FiltersUpdateEvent';

// No Content events
const NO_RESULTS = 'NoResultsEvent';
const HAS_RESULTS = 'HasResultsEvent';



export { 
  ADD_FILTER,
  REMOVE_FILTER,
  SALES_FILTER,
  FILTER_RESET,
  FILTER_SELECTED,
  RESULTS_UPDATE,
  FILTERS_UPDATE,
  NO_RESULTS,
  HAS_RESULTS 
}

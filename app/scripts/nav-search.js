import {removeElement, hasClass, getInnerHtml, fetchWithTimeout} from './utils.js';

const navSearchInit = () => {

    const input = document.querySelector('.autoComplete');
    const inputMobile = document.querySelector('.search-mobile');

    const formDesktop = document.querySelector('#desktop-search-form');
    const formMobile = document.querySelector('#mobile-search-form');

    const autocompleteWrapper = document.querySelector('.cs-autocomplete-wrapper-desktop');
    const autocompleteWrapperMobile = document.querySelector('.cs-autocomplete-wrapper-mobile');

    const closeButtonDesktop = document.querySelector('.cs-input-close-desktop');
    const closeButtonMobile = document.querySelector('.cs-input-close-mobile');

    const cancelLinkMobile = document.querySelector('.cs-cancel-link');

    const desktopToggleForm = document.querySelector('.cs-desktop-toggle-form');
    const mobileToggleForm = document.querySelector('.cs-mobile-toggle-form');

    const dataContainer = document.querySelector('.cs-autocomplete-data'); // Data Mandatory for the search bar autcomplete
    const noResults = document.querySelector('.cs-autocomplete-no-results-data'); // data html used for mobile and desktop
    const noProductAvailableBanner = document.querySelector('.cs-autocomplete-product-not-available-data'); // data html used for mobile and desktop
    const didYouMeanLabel = document.querySelector('.cs-autocomplete-did-you-mean-data'); // data html used for mobile and desktop

    const jsonUrl = dataContainer.dataset.autocompleteJson; // API JsonUrl entry point
    const jsonTimeout = parseInt(dataContainer.dataset.jsonApiDelay) || 5000; // Json call timeout 5s by default

    const loadingStateAriaText = getInnerHtml(document.querySelector('.cs-loading-state-aria-text')); // Loading State Aria message
    const ariaFragmentSentence = getInnerHtml(document.querySelector('.cs-aria-fragment-sentence')); // Truncated sentence Aria message
    const dataAriaSuggestionsDisplayed = dataContainer.dataset.AriaSuggestionsDisplayed || 'Search suggestions displayed';
    const dataAriaSuggestionsClosed = dataContainer.dataset.AriaSuggestionsClosed || 'Search suggestions closed';

    const threshold = parseInt(dataContainer.dataset.threshold) || 2; // Min. Chars length to start Engine
    const debounceTiming = parseInt(dataContainer.dataset.debounceTiming) || 800; // Post duration for engine to start in ms
    const maximumItemsByCategory = parseInt(dataContainer.dataset.maximumItemsByCategory) || 3; // Max items by category displayed
    const maximumCategory = parseInt(dataContainer.dataset.maximumCategory) || 5; // max Categories displayed

    const characterCountLimit = {
        desktop: parseInt(dataContainer.dataset.characterCountLimitDesktop) || 40,
        mobile: parseInt(dataContainer.dataset.characterCountLimitMobile) || 30
    }

    let inputValue = '';
    let dropdownOpen = false;
    let timeout = null;

    const displayLoadingState = (autoCompleteContainer) => {
        autoCompleteContainer.querySelector('.autocomplete-inner').innerHTML = `
            <li class='cs-loading-state'>
                <span class='visuallyhidden'>${loadingStateAriaText}</span>
                <div class='cs-loading-state-container' aria-hidden='true'>
                    <div class="cs-loading-state-title"></div>
                    <div class="cs-loading-state-p"></div>
                    <div class="cs-loading-state-p"></div>
                    <div class="cs-loading-state-p"></div>
                    <div class="cs-loading-state-title"></div>
                    <div class="cs-loading-state-p"></div>
                    <div class="cs-loading-state-p"></div>
                    <div class="cs-loading-state-p"></div>
                    <div class="cs-loading-state-title"></div>
                    <div class="cs-loading-state-p"></div>
                    <div class="cs-loading-state-p"></div>
                    <div class="cs-loading-state-p"></div>
                </div>
            </li>
        `;
    }

    const closeButtonDisplay = (event, formContainer) => {
        if (event && event.target && event.target.value.length > 0) {
            showCloseButton(formContainer);
        } else {
            hideCloseButton(formContainer);
        }
    }

    const showCloseButton = (formContainer) => {
        const thisInputClose = formContainer.querySelector('.cs-input-close');
        thisInputClose.classList.add('show');
    }

    const hideCloseButton = (formContainer) => {
        const thisInputClose = formContainer.querySelector('.cs-input-close');
        thisInputClose.classList.remove('show');
    }


    const hightlight = (string) => {
        return string.replace(new RegExp(inputValue, 'gi'), (match) => `<mark class='cs-hightlight'>${match}</mark>`);
    }

    const truncatedResult = (string, isMobile = false) => {
        let result = string.replace(/(<([^>]+)>)/ig, '');
        const lowercaseInputValue = inputValue.toLowerCase();
        const lowercaseString = result.toLowerCase();
        const maxCharacters = isMobile ? characterCountLimit.mobile : characterCountLimit.desktop // TODO GESTURE MOBILE/DESKTOP

        if (result.length > maxCharacters && lowercaseString.indexOf(lowercaseInputValue) != -1) {
            //Enter if only results more than 35 characters

            const stringBefore = lowercaseString.split(lowercaseInputValue)[0].length;
            const stringAfter = lowercaseString.split(lowercaseInputValue)[1].length;
            const queryLength = inputValue.length;
            const calculhalf = Math.floor((maxCharacters - queryLength - 6) / 2);

            if (stringBefore < maxCharacters) {
                // If query in fist 30 characters
                result = result.substring(0, maxCharacters - 3) + '...';

            } else if (stringBefore > maxCharacters && stringAfter < maxCharacters) {
                // If query in last 30 characters
                result = '...' + result.substring((result.length - maxCharacters + 3), result.length);

            } else {
                // If it is not in the first and last 30 characters 
                result = '...' + result.substring((stringBefore - calculhalf), (result.length - stringAfter) + calculhalf) + '...';
            }

            result = `<span class='visuallyhidden'>${ariaFragmentSentence}</span>${result}`;

        } else if (result.length > maxCharacters) {
            
            result = `<span class='visuallyhidden'>${ariaFragmentSentence}</span>${result.substring(0, maxCharacters - 3)}...`;
        }

        return result;
    }

    const renderKeywords = (hits, isMobile) => {
        if (hits) {
            return `
                <ul>
                    ${hits.slice(0, maximumItemsByCategory).map(hit => {

                        // Truncated if text > maxCharacters
                        let name = truncatedResult(hit.name, isMobile);
                        // string with match element inside:
                        name = hightlight(name);
                    
                        return ` 
                            <li>
                                <a href='${hit.productUrl}'>${name}</a> 
                            </li>
                        `}).join('')}
                </ul>
            `;
        }
    }

    const markup = (element, isMobile) => {
        if (element) {
            return `
                <li class='cs-autocomplete-wrapper-item'>
                    <h3>${element.category}</h3>
                    ${renderKeywords(element.hits, isMobile)}
                </li>
            `
        }
    }

    const seeAll = () => {
        return `
            <li class='cs-autocomplete-wrapper-item'>
                ${getInnerHtml(document.querySelector('.cs-see-all-link'))}
            </li>
        `
    }

    const createNoResultsHtml = (autoCompleteContainer) => {
        const listContainer = autoCompleteContainer.querySelector('.autocomplete-inner');
        listContainer.innerHTML = `
                ${getInnerHtml(noResults)}
        `
    }

    const ariaExpandedTrue = (formContainer) => {
        if (formContainer) {
            formContainer.querySelector('.autocomplete-inner').setAttribute('aria-expanded','true');

            if (!dropdownOpen) {
                let myMessage = document.createElement('p');
                myMessage.setAttribute('aria-live', 'assertive');
                let myAlertText = document.createTextNode(dataAriaSuggestionsDisplayed);
                myMessage.appendChild(myAlertText);
                document.body.appendChild(myMessage); 
                myMessage.classList.add('assertive');
                setTimeout(() => {
                    removeElement(myMessage)
                }, 4000);
            }
        }
    }

    const ariaExpandedFalse = (formContainer) => {

        if (formContainer) {
            formContainer.querySelector('.autocomplete-inner').setAttribute('aria-expanded','false');
    
            let myMessage = document.createElement('p');
            myMessage.classList.add('assertive');
            myMessage.setAttribute('aria-live', 'assertive');
            let myAlertText = document.createTextNode(dataAriaSuggestionsClosed);
            myMessage.appendChild(myAlertText);
            document.body.appendChild(myMessage); 
            setTimeout(() => {
                removeElement(myMessage)
            }, 4000);
        }
    }

    const setAutocompleteWrapperHeight = (autoCompleteContainer) => {
        let autcompleteInnerElement = autoCompleteContainer.querySelector('.autocomplete-inner')
        if (autcompleteInnerElement) {
            autoCompleteContainer.style.height = `${autcompleteInnerElement.scrollHeight}px`;
            autoCompleteContainer.style.minHeight = `${autcompleteInnerElement.scrollHeight || '500'}px`;
        }
    }

    const setAutocompleteWrapperHeight0 = (autoCompleteContainer) => {
        autoCompleteContainer.style.height = '0';
        autoCompleteContainer.style.minHeight = '0';
    }

    const focus = (element) => {
        setTimeout(() => element.focus(), 400);
    }

    const handleEscape = (event) => {
        event = event || window.event;
        if (event) {
            if (event.keyCode == 27) {
                resetInput();
            }
        }
    }

    let clearResults = (autoCompleteContainer) => {
        autoCompleteContainer.querySelector('.autocomplete-inner').innerHTML = '';
    }

    const resetInput = (isMobile) => {
        inputValue = '';
        inputMobile.value = '';
        input.value = '';
        clearResults(autocompleteWrapperMobile);
        setAutocompleteWrapperHeight0(autocompleteWrapperMobile);
        clearResults(autocompleteWrapper);
        setAutocompleteWrapperHeight0(autocompleteWrapper);
        hideCloseButton(formDesktop);
        hideCloseButton(formMobile);
        if (isMobile) {
            formMobile.querySelector('.cs-input-search-link').setAttribute('aria-disabled', 'true');
            focus(inputMobile);
        } else if (isMobile == false) {
            formDesktop.querySelector('.cs-input-search-link').setAttribute('aria-disabled', 'true');
            focus(input);
        }
        console.log(dropdownOpen)
        if (dropdownOpen) {
            // Tell user if the autocomplete is close
            ariaExpandedFalse(isMobile ? formMobile : formDesktop);
        }
        dropdownOpen = false;
    }

    const resetInputMobile = (withFocus) => {
        dropdownOpen = false;
        inputValue = '';
        inputMobile.value = '';
        clearResults(autocompleteWrapperMobile);
        setAutocompleteWrapperHeight0(autocompleteWrapperMobile);
        hideCloseButton(formMobile);
        if (withFocus) {
            focus(inputMobile);
        }
    }

    const resetInputDesktop = (withFocus) => {
        dropdownOpen = false;
        inputValue = '';
        input.value = '';
        clearResults(autocompleteWrapper);
        setAutocompleteWrapperHeight0(autocompleteWrapper);
        hideCloseButton(formDesktop);
        if (withFocus) {
            focus(input);
        }
    }

    let closeMobileForm = () => {
        const searchToggleBtn = document.querySelector('.cs-mobile-toggle-form');
        searchToggleBtn.classList.remove('show');
        searchToggleBtn.setAttribute('aria-expanded', 'false');
        formMobile.setAttribute('aria-hidden', 'true');
        formMobile.classList.remove('show');
        focus(searchToggleBtn);
    }

    const toggleSearchTablet = (form, isMobile = false) => {
        const element = event.currentTarget;
        const isShow = hasClass(element, 'show');

        if (isShow) {
            element.classList.remove('show');
            element.setAttribute('aria-expanded', 'false');
            form.setAttribute('aria-hidden', 'true');
            form.classList.remove('show');
        } else {
            element.classList.add('show');
            element.setAttribute('aria-expanded', 'true')
            form.setAttribute('aria-hidden', 'false')
            form.classList.add('show');
            if (isMobile) {
                focus(inputMobile);
            }
        }
    }

    const setProductBanner = (isSynonyms, formContainer, autoCompleteContainer ,thisInput) => {

        if (isSynonyms === 'Y') {
            // Click button product banner remover handle
            const closeProductBannerButton = formContainer.querySelector('.cs-close-product-banner');
            if (closeProductBannerButton) {
                closeProductBannerButton.addEventListener('click', (event) => {
                    event.preventDefault();
                    const item = autoCompleteContainer.querySelector('.cs-autocomplete-product-not-available-item');
                    const parentItem = autoCompleteContainer.querySelector('.autocomplete-inner');
                    parentItem.removeChild(item);
                    autoCompleteContainer.style.height = 'auto';
                    autoCompleteContainer.style.minHeight = '0';
                    setAutocompleteWrapperHeight(autoCompleteContainer);
                    focus(thisInput);
                });
            }
        }
    }

    const createResultsList = (data, autoCompleteContainer, isMobile) => {
        let newElements = '';
        const results = data.results;
        const dataLength = results.length;
        const listContainer = autoCompleteContainer.querySelector('.autocomplete-inner');
        const isSuggestedTerm = data.suggestedTerm && data.suggestedTerm !== '' ? true : false;
        const isProductNotAvailable = data.isSynonyms === 'Y' ? true : false;

        if (listContainer.length === 0) return;

        if (isProductNotAvailable) {
            // Add product not available banner if 'Y' in json response isSynonyms
            newElements += getInnerHtml(noProductAvailableBanner);
        }

        if (isSuggestedTerm) {
            newElements += getInnerHtml(didYouMeanLabel, `<button class='cs-suggested-term'>${data.suggestedTerm}</button>`);
        }

        // Loop throught category and create markup - break when maximum items reach
        for (let index = 0; index < dataLength; index++) {
            if (index == maximumCategory) {
                break;
            }

            const element = results[index];
            if (typeof element !== 'undefined' || element !== null) {
                newElements += markup(element, isMobile);
            }
        }

        // Add see-all link
        if (!isSuggestedTerm) {
            newElements += seeAll();
        }

        // print autocomplete html in the selected div
        listContainer.innerHTML = newElements;

        if(isSuggestedTerm) {
            // bind button suggested term and recall json autcomplete handler
            autoCompleteContainer.querySelector('.cs-suggested-term').addEventListener('click', (event) => {
                event.preventDefault();
                if (isMobile) {
                    inputMobile.focus();
                    inputMobile.value = data.suggestedTerm;
                    inputMobile.dispatchEvent(new CustomEvent('input'));
                } else {
                    input.focus();
                    input.value = data.suggestedTerm;
                    input.dispatchEvent(new CustomEvent('input'));
                }
            });
        }
    }

    const autocompleteHandler = (event, isMobile) => {

        // IE Fix placeholder bad behavior
        if(input.value === input.getAttribute('placeholder')) return;

        let formContainer = isMobile ? formMobile : formDesktop;
        let autoCompleteContainer = isMobile ? autocompleteWrapperMobile : autocompleteWrapper;
        let thisInput = isMobile ? inputMobile : input;
        const searchBtn = formContainer.querySelector('.cs-input-search-link');

        closeButtonDisplay(event, formContainer);

        //Remove last results
        clearResults(autoCompleteContainer);

        if (event && event.target && event.target.value.length > 1) {
            searchBtn.setAttribute('aria-disabled', 'false');
        } else {
            searchBtn.setAttribute('aria-disabled', 'true');
        }

        if (event && event.target && event.target.value.length > threshold) {

            inputValue = event.target.value;

            let fetchUrl = `${jsonUrl}${inputValue}`

            // Loading state
            displayLoadingState(autoCompleteContainer);

            fetchWithTimeout(fetchUrl, jsonTimeout).then(function(data) {
                // process response

                if (data.results.length > 0) {
                    // if it has results create a list with data
                    createResultsList(data, autoCompleteContainer, isMobile);

                    // Set the new height of the container
                    setAutocompleteWrapperHeight(autoCompleteContainer);

                    // Set a11y aria
                    ariaExpandedTrue(formContainer);

                    // Set product banner not available if isSynonyms = 'Y'
                    setProductBanner(data.isSynonyms, formContainer, autoCompleteContainer, thisInput);

                } else {
                    // If no results create no result text
                    createNoResultsHtml(autoCompleteContainer);

                    // Set the height of the container
                    setAutocompleteWrapperHeight(autoCompleteContainer);
                }

                dropdownOpen = true;

            }).catch(function(error) {
                console.log('might be a timeout error - do nothing');
                clearResults(autoCompleteContainer);
            })

        } else if (dropdownOpen) {
            // Close the autocomplete dropdown
            setAutocompleteWrapperHeight0(autoCompleteContainer);
            clearResults(autoCompleteContainer);
            ariaExpandedFalse(formContainer);
            dropdownOpen = false;
        }
    }

    const initAutocomplete = () => {

        if (input) {

            input.addEventListener('input', (event) => {
                // Clear the timeout if it has already been set.
                // This will prevent the previous task from executing
                // if it has been less than <MILLISECONDS>

                // for IE11 placeholder problem
                // Because input placeholder is put in value in IE11;
                if(input.value === input.getAttribute('placeholder')) return;
                
                clearTimeout(timeout);
                let isMobile = false;
                timeout = setTimeout(function () {
                    autocompleteHandler(event, isMobile);
                }, debounceTiming);
            });

            input.addEventListener('focus', (event) => {
                // force input value on IE to stay empy on focus
                // IE Fix placeholder bad behavior
                if(input.value === input.getAttribute('placeholder')) {
                    input.value = '';
                }
                
            });
        }

        if (inputMobile) {
            inputMobile.addEventListener('input', (event) => {
                // Clear the timeout if it has already been set.
                // This will prevent the previous task from executing
                // if it has been less than <MILLISECONDS>

                // for IE11 placeholder problem
                // Because input placeholder is put in value in IE11;
                if(input.value === input.getAttribute('placeholder')) return;

                clearTimeout(timeout);
                let isMobile = true;
                timeout = setTimeout(function () {
                    autocompleteHandler(event, isMobile);
                }, debounceTiming);
            });
        }

        document.body.addEventListener('keydown', function (event) {
            if (dropdownOpen) {
                //Only activate escape clear input if the dropdown is open
                handleEscape(event);
            }
        });

        if (closeButtonDesktop) {
            // Clear the input field on desktop
            closeButtonDesktop.addEventListener('click', (event) => {
                event.preventDefault();
                resetInput(false);
            });
        }

        if (closeButtonMobile) {
            // Clear the input field on mobile
            closeButtonMobile.addEventListener('click', (event) => {
                event.preventDefault();
                resetInput(true);
            });
        }
        
        if (cancelLinkMobile) {
            cancelLinkMobile.addEventListener('click', (event) => {
                // Close the Search on mobile
                event.preventDefault();
                closeMobileForm()
            });
        }

        if (desktopToggleForm) {
            desktopToggleForm.addEventListener('click', (event) => {
                // Toogle the tablet Form, only between 1260px and 960px
                event.preventDefault();
                toggleSearchTablet(formDesktop);
            });
        }

        if (mobileToggleForm) {
            mobileToggleForm.addEventListener('click', (event) => {
                // Toogle the tablet Form, only between 1260px and 960px
                event.preventDefault();
                toggleSearchTablet(formMobile, true);
            });
        }

        if (formDesktop) {
            formDesktop.querySelector('.cs-input-search-link').addEventListener('click', (event) => {
                // Prevent the button to post the form, 
                // in the purpose to execute the global Function runSearch();
                event.preventDefault();
            });
        }

        if (formMobile) {
            formMobile.querySelector('.cs-input-search-link').addEventListener('click', (event) => {
                // Prevent the button to post the form on mobile, 
                // in the purpose to execute the global Function runSearch();
                event.preventDefault();
            });
        }


    }
    

    initAutocomplete();
}

export {navSearchInit}
## Getting Started

- `node` version : 12.9.1
- `yarn` version : 1.21.1
- This project can also be run with npm; `npm` version: 6.13.4/6.14.4

- Run `yarn start` to preview and watch for changes
- Run `yarn start -- --port=8080` to preview and watch for changes in port `8080`
- Run `yarn install --save <package>` to install dependencies, frontend included
- Run `yarn run  serve:test` to run the tests in the browser
- Run `yarn run  serve:test -- --port=8085` to run the tests in the browser in port `8085`
- Run `yarn run build` to build your webapp for production
- Run `yarn run serve:dist` to preview the production build
- Run `yarn run serve:dist -- --port=5000` to preview the production build in port `5000`